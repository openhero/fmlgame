# Fantasy Mythical Legend

A game based on the OpenHero engine.

Battle with heroes from legend against gods and demons, and claim your righteous throne as the One True FML!

This repository contains the assets for a distinct OpenHero game, you will need an OpenHero server to deploy the game.
